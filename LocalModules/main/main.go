package main

import (
	"fmt"

	"local/slices"
)

func main() {
	a := slices.New(1, 2)
	b := slices.Repeat(a, 3)
	fmt.Println(b)
}
