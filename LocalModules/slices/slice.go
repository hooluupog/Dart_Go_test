package slices

func New(value int, Len int) []int {
	a := make([]int, Len)
	for i, _ := range a {
		a[i] = value
	}
	return a
}

func Repeat(a []int, times int) []int {
	var b []int
	for i := 0; i < times; i++ {
		b = append(b, a...)
	}
	return b
}
