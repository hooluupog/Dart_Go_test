// Import BenchmarkBase class.
import 'package:benchmark_harness/benchmark_harness.dart';

const int N = 46;

// Create a new benchmark by extending BenchmarkBase
class Fib extends BenchmarkBase {
  const Fib() : super("fib");

  static void main() {
    Fib().report();
  }

  // The benchmark code.
  void run() {
    fib(N);
  }

  int fib(int n) {
    if (n == 1 || n == 2) return 1;
    return fib(n - 1) + fib(n - 2);
  }
}

class Fib_tail extends BenchmarkBase {
  const Fib_tail() : super("fib_tail");

  static void main() {
    Fib_tail().report();
  }

  void run() {
    fib_tail(N, 0, 1);
  }

  int fib_tail(int n, int a, int b) {
    return n == 0 ? a : fib_tail(n - 1, b, a + b);
  }
}

class Fib_mem extends BenchmarkBase {
  const Fib_mem() : super("fib_mem");

  static void main() {
    Fib_mem().report();
  }

  void run() {
    cache.fillRange(0, cache.length, 0);
    fib_mem(N, cache);
  }

  static List<int> cache = List.filled(N, 0);

  int fib_mem(int n, List<int> cache) {
    if (n == 1 || n == 2) {
      return 1;
    }
    if (cache[n - 1] == 0) {
      cache[n - 1] = fib_mem(n - 1, cache);
    }
    if (cache[n - 2] == 0) {
      cache[n - 2] = fib_mem(n - 2, cache);
    }
    return cache[n - 1] + cache[n - 2];
  }
}

class Fib_loop extends BenchmarkBase {
  const Fib_loop() : super("fib_loop");

  static void main() {
    Fib_loop().report();
  }

  void run() {
    fib_loop(N);
  }

  int fib_loop(int n) {
    if (n == 0) return 0;
    if (n == 1) return 1;
    var res = 0, a = 1, b = 0;
    for (int i = 2; i <= n; i++) {
      res = a + b;
      b = a;
      a = res;
    }
    return res;
  }
}

main() {
  // Run FibBenchmark
  Fib.main();
  Fib_mem.main();
  Fib_tail.main();
  Fib_loop.main();
}
