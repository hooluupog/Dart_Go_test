@JS()
library state;

import 'package:js/js.dart';

@JS()
@anonymous
class Step {
  external int get count;
  external int get n;
  external String get x;
  external String get y;
  external String get z;
  external bool get over;
  external factory Step({int count, int n, String x, String y, String z, bool over});
}

@JS()
@anonymous
class State {
  external int get disknum;
  external bool get over;
  external factory State({int disknum, bool over});
}

@anonymous
@JS()
abstract class MessageEvent {
  external dynamic get data;
}

@JS('postMessage')
external void PostMessage(obj);

@JS('onmessage')
external void set onMessage(f);

@JS("JSON.stringify")
external String stringify(obj);

