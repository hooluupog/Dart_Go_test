import 'dart:html';
import 'package:hanoi/state.dart';

void main() {
  final worker = Worker('hanoi.dart.js');
  worker.onMessage.listen((e) {
    if (e.data is String) {
      querySelector('#sample_text_id').innerHtml += e.data;
    } else {
      var msg = Step(
          over: e.data['over'],
          count: e.data['count'],
          n: e.data['n'],
          x: e.data['x'],
          z: e.data['z']);
      if (msg.over) {
        worker.postMessage(State(over: true));
      } else {
        var data =
            '<li>${msg.count}.move disk ${msg.n} from ${msg.x} to ${msg.z}</li>';
        querySelector('#sample_text_id').innerHtml += data;
      }
    }
  });
  var disknum = 0;
  querySelector('#title_id').text = 'Tower Of Hanoi disk numbers = $disknum';
  var runbutton = querySelector('#run');
  runbutton.onClick.listen((e) {
    disknum++;
    querySelector('#title_id').text = 'Tower Of Hanoi disk numbers = $disknum';
    worker.postMessage(State(disknum: disknum, over: false));
  });
  var clearbutton = querySelector('#clear');
  clearbutton.onClick.listen((e) {
    disknum = 0;
    querySelector('#title_id').text = 'Tower Of Hanoi disk numbers = $disknum';
    querySelector('#sample_text_id').innerHtml = '';
  });
}
