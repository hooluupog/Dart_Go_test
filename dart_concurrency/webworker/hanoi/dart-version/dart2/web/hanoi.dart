import 'package:js/js.dart';

import 'package:hanoi/state.dart';

var count = 0;

void main() {
  var sw = Stopwatch();
  onMessage = allowInterop((event) {
    var e = event as MessageEvent;
    var msg = e.data as State;
    if (msg.over) {
      sw.stop();
      print('${sw.elapsedMilliseconds}ms.');
      var text = '<li>time used: ${sw.elapsedMilliseconds}ms.</li><br>';
      PostMessage(text);
    } else {
      sw.reset();
      sw.start();
      count = 0;
      hanoi(msg.disknum, 'A', 'B', 'C');
      PostMessage(Step(over: true));
    }
  });
}

void hanoi(int n, String x, y, z) {
  if (n == 1) {
    count++;
    var text = Step(count: count, n: n, x: x, z: z, over: false);
    PostMessage(text);
  } else {
    hanoi(n - 1, x, z, y);
    count++;
    var text = Step(count: count, n: n, x: x, z: z, over: false);
    PostMessage(text);
    hanoi(n - 1, y, x, z);
  }
}
